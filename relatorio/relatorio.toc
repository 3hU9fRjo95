\select@language {brazil}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {chapter}{\numberline {2}Primeiros Passos}{4}
\contentsline {section}{\numberline {2.1}Instala\IeC {\c c}\IeC {\~a}o}{4}
\contentsline {section}{\numberline {2.2}Utiliza\IeC {\c c}\IeC {\~a}o}{4}
\contentsline {chapter}{\numberline {3}Mecanismos de Programa\IeC {\c c}\IeC {\~a}o Concorrente}{5}
\contentsline {section}{\numberline {3.1}Gorotinas}{5}
\contentsline {section}{\numberline {3.2}Channel}{5}
\contentsline {section}{\numberline {3.3}go}{6}
\contentsline {section}{\numberline {3.4}select}{6}
\contentsline {chapter}{\numberline {4}Pr\IeC {\'a}tica}{8}
\contentsline {section}{\numberline {4.1}Compilando e executando os exemplos}{8}
\contentsline {section}{\numberline {4.2}Hello, World!}{8}
\contentsline {section}{\numberline {4.3}Fun\IeC {\c c}\IeC {\~o}es Auxiliares}{8}
\contentsline {section}{\numberline {4.4}Constantes}{10}
\contentsline {section}{\numberline {4.5}Destrui\IeC {\c c}\IeC {\~a}o (de vari\IeC {\'a}veis)}{10}
\contentsline {section}{\numberline {4.6}Mutex}{10}
\contentsline {section}{\numberline {4.7}Sem\IeC {\'a}foro}{11}
\contentsline {section}{\numberline {4.8}Monitor}{13}
\contentsline {section}{\numberline {4.9}Vari\IeC {\'a}vel de Condi\IeC {\c c}\IeC {\~a}o}{15}
\contentsline {section}{\numberline {4.10}The Go Way}{17}
\contentsline {section}{\numberline {4.11}M\IeC {\'u}ltiplos Produtores e Consumidores}{18}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{20}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{21}
