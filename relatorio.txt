Golang
	Instala��o:
		cd ~
		gedit .bashrc &
			colar no final:
				export GOROOT=$HOME/go
				export GOARCH=386
				export GOOS=linux
			reiniciar o shell
			sudo apt-get install bison gcc libc6-dev ed gawk make
			sudo apt-get install python-setuptools python-dev build-essential gcc
			sudo easy_install mercurial
			hg clone -r release https://go.googlecode.com/hg/ $GOROOT
			cd $GOROOT/src
			mkdir $HOME/bin
			./all.bash
			cd $HOME/bin
			sudo mv * /bin
	Uso:
		editor de texto -> prog.go
		8g prog.go -> prog.8
		8l -o prog prog.8 -> prog
		./prog 

Mecanismos de Programa��o Concorrente:
  Channel:
		Representa um cana de comunica��o / sincroniza��o que pode conectar duas computa��es concorrentes. Na realidade s�o refer�ncias para um objeto que coordena a comunica��o.
		<- � o operador bin�rio de comunica��o (envio)
		<- tamb�m � o operador, desta vez un�rio, de recebimento
		ambos s�o at�micos
		Opera��es em canais bloqueiam, ent�o se n�o houver receptor para uma mensagem, a opera��o de envio espera at� que algum apare�a.
		pode ser receive_only (<-chan) e send_only (chan<-). Tod canais s�o criados bidirecionais, a utilidade disso � para os argumentos dos fun��es para garantir que ele s� vai ler (respectivamente escrever) no canal.
		um canal as�ncrono com buffer � criado passando o n�mero de elementos no buffer para o make (c := make(chan int, 50)).
		para testar comunicabilidade:
		ok := c <- v;
		v, ok = <-c;
		um channel com buffer pode ser usado como um sem�foro

	Goroutines:
		Go tem seu pr�prio modelo de processo/thread/processo leve/corotinas, chamado goroutines. No gccgo s�o implementadas com pthreads, mas no 6g n�o. S�o divididas de acordo com o necess�rio em threads do sistema. Quando uma goroutine executa uma chamada de sistema bloqueane, nenhuma outra goroutine � bloqueada.
		$GOMAXPROCS ou runtime.GOMAXPROCS(n) dizem quantas goroutines n�o bloqueantes entre si o sistema pode executar ao mesmo tempo.

	go:
		� o operador que inicia a execu��o concorrente de uma goroutine no mesmo espa�o de endere�amento.
		Prefixe uma chamada de fun��o ou de m�todo com a palavra-chave go para executar a chamada numa nova gorotina. Quando a chamada termina, a gorotina tamb�m termina (silenciosamente). O efeito � similar a nota��o & do shell Unix para rodar um comando em background.

	select:
		� um estrutura de controle an�loga a um switch de comunica��es. Cada caso deve ser um send ou receive. escolhe qual das comunica��es listadas em seus casos pode prosseguir. Se todas est�o bloqueadas, ele espera at� que alguma desbloqueie. Se v�rias podem prosseguir, ele escolhe uma aleatoriamente.
		sum�rio:
			todos os casos devem ser uma comunica��o
			todas as express�es com canal s�o avaliadas
			todas as express�es a serem enviadas s�o avaliadas
			se qualquer comunica��o pode prosseguir, ela o faz, e as outras s�o ignoradas
			caso contr�rio:
				se h� uma cl�usula default, ela executa
				se n�o h� default, o select bloqueia at� que alguma comunica��o possa prosseguir. n�o h� re-avalia��o de canais ou valores
			se v�rias cl�usulas est�o prontas, uma � escolhida aleatoriamente, de maneira justa, e as outras n�o executam.
			random bit generator
				for {
					select {
					case c <- 0:
					case c <- 1:
					}
				}


	defer executa uma fun��o apenas quando a fun��o que o chama retorna. �til para destrancar mutexes.

	Filosofia: "n�o comunique compartilhando mem�ria. Em vez disso, compartilhe mem�ria comunicando-se." O simples ato de comunica��o garante a sincroniza��o.

	Concorr�ncia em muitos ambientes se torna dif�cil pelas sutilezas envolvidas no acesso correto a vari�veis compartilhadas. Go encoraja uma abordagem diferente, na qual vari�veis compartilhadas s�o passadas atrav�s de canais e nunca s�o de fato ativamente compartilhadas por threads de execu��o separadas. Apenas uma gorotina tem acesso � vari�vel num dado momento de tempo. Condi��es de corrida relativas aos dados n�o podem ocorrer, por constru��o.

	concorr�ncia inspirada no CSP.

	Exclus�o M�tua:
		Cria��o:
		Uso:
		Destrui��o:
		Implementa��o:
			Problema produtor-consumidor com buffer limitado e taxa de 
	produ��o e consumo aleat�rias.
	Sem�foros:
		Cria��o:
		Uso:
		Destrui��o:
		Implementa��o:
			Problema produtor-consumidor com buffer limitado e taxa de 
	produ��o e consumo aleat�rias.
	Monitores:
		Cria��o:
		Uso:
		Destrui��o:
		Implementa��o:
			Problema produtor-consumidor com buffer limitado e taxa de 
	produ��o e consumo aleat�rias.
	Vari�veis de Condi��o:
		Cria��o:
		Uso:
		Destrui��o:
		Implementa��o:
			Problema produtor-consumidor com buffer limitado e taxa de 
	produ��o e consumo aleat�rias.

Processos, Threads ou Ambos?
	
Fun��es �teis:
	time.Sleep(int)
	func init()
	http://golang.org/pkg/sync/
	http://golang.org/doc/go_mem.html
