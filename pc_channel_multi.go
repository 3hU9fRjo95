/* pc_channel_multi.go */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
	"flag"
)

func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

func random_sleep () {
	time.Sleep(random_between(0, *MAX_SLEEP_TIME) * 10e7)
}

func send (channel chan int, i int) {
	channel <- i
}

func recv (channel chan int) int {
	return <-channel
}

func signal (channel chan int) {
	if len(channel) < cap(channel) || cap(channel) == 0 {
		channel <- 0
	}
}

func wait (channel chan int) {
	<-channel
}

var (
	BUFFER_SIZE = flag.Int("s", 5, "BUFFER_SIZE") // flag -s
	MAX_SLEEP_TIME = flag.Int64("t", 5, "MAX_SLEEP_TIME") // flag -t
	MAX_PRODUCTIONS = flag.Int("m", 0, "MAX_PRODUCTIONS, 0 for infinity") // flag -m
	N_PRODUCERS = flag.Int("p", 3, "N_PRODUCERS") // flag -p
	N_CONSUMERS = flag.Int("c", 3, "N_CONSUMERS") // flag -c
	pipe = make(chan int, *BUFFER_SIZE)
	quit 	= make(chan int)
)

func producer (id int) {
	for i := 0; i < *MAX_PRODUCTIONS || *MAX_PRODUCTIONS == 0; i++ {
		Println(id, "producing:", i + id * 1e5)
		send(pipe, i + id * 1e5)
		random_sleep()
	}
	signal(quit)
}

func consumer (id int) {
	for {
		v := recv(pipe)
		if closed(pipe) { // canal fechado?
			break
		}
		Println("\t\t\t", id, "consuming:", v)
		random_sleep()
	}
	signal(quit)
}

func main () {
	flag.Parse() // processa flags
	Println("BUFFER_SIZE =", *BUFFER_SIZE)
	Println("MAX_SLEEP_TIME =", *MAX_SLEEP_TIME)
	Println("MAX_PRODUCTIONS =", *MAX_PRODUCTIONS)
	Println("N_PRODUCERS =", *N_PRODUCERS)
	Println("N_CONSUMERS =", *N_CONSUMERS)
	for i := 0; i < *N_PRODUCERS; i++ { // cria produtores
		go producer(i + 1)
	}
	for i := 0; i < *N_CONSUMERS; i++ { // cria consumidores
		go consumer(i + 1)
	}
	for i := 0; i < *N_PRODUCERS; i++ { // espera o término dos produtores
		wait(quit)
	}
	close(pipe)	// fecha comunicação
	for i := 0; i < *N_CONSUMERS; i++ {	// espera o término dos consumidores
		wait(quit)
	}
}

