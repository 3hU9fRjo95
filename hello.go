/* hello */

package main // executável principal sempre deve pertencer a este pacote

import . "fmt" // importa funções de saída formatada (Printf)

// função principal (como em C)
func main() {
	//sem o . no import teríamos que utilizar fmt.Printf
	Printf("hello, world\n")
}

