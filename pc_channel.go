/* pc_channel */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
)

// gera um número inteiro aleatório no intervalo [begin, end]
func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

// dorme durante um intervalo de tempo aleatório
func random_sleep () {
	time.Sleep(random_between(0, MAX_SLEEP_TIME) * 10e7)
}

// envia o inteiro i através do canal channel
func send (channel chan int, i int) {
	channel <- i
}

// recebe um inteiro através do canal channel e o retorna
func recv (channel chan int) int {
	return <-channel
}

// envia um valor qualquer para o canal channel
func signal (channel chan int) {
	// garante que a chamada só seja síncrona caso channel seja síncrono, ou seja,
  // caso channel seja assíncrono e esteja com o buffer cheio não envia nada
	if len(channel) < cap(channel) || cap(channel) == 0 {
		send(channel, 0) // poderia ser qualquer valor
	}
}

// recebe um valor qualquer do canal channel
func wait (channel chan int) {
	recv(channel) // ignora o valor recebido
}

const (
	MAX_SLEEP_TIME = 5
	BUFFER_SIZE = 5
)

var pipe = make(chan int, BUFFER_SIZE)

func producer () {
	for i := 0; ; i++ { // laço infinito
		Println("producing:", i)
		send(pipe, i)
		random_sleep()
	}
}

func consumer () {
	for { // laço infinito
		v := recv(pipe)
		Println("\t\t\tconsuming:", v)
		random_sleep()
	}
}

func main () {
	go producer()
	go consumer()
	wait(make(chan int)) // espera indefinidamente
}

