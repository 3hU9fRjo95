/* pc_sem.go */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
)

func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

func random_sleep () {
	time.Sleep(random_between(0, MAX_SLEEP_TIME) * 10e7)
}

func signal (channel chan int) {
	if len(channel) < cap(channel) || cap(channel) == 0 {
		channel <- 0
	}
}

func wait (channel chan int) {
	<-channel
}

const (
	MAX_SLEEP_TIME = 5
	BUFFER_SIZE = 5
)

var (
	mutex = make(chan int, 1);
	empty, full chan int;
	buffer []int;
	front, rear = 0, 0
)

func producer () {
	for i := 0; ; i++ { // laço infinito
		wait(empty)
		wait(mutex)
			Println("producing:", i)
			buffer[rear] = i
			rear = (rear + 1) % BUFFER_SIZE
		signal(mutex)
		signal(full)
		random_sleep()
	}
}

func consumer () {
	for { // laço infinito
		wait(full)
		wait(mutex)
			v := buffer[front]
			front = (front + 1) % BUFFER_SIZE
			Println("\t\t\tconsuming:", v)
		signal(mutex)
		signal(empty)
		random_sleep()
	}
}

func main () {
	buffer = make([]int, BUFFER_SIZE)
	empty = make(chan int, BUFFER_SIZE);
	full = make(chan int, BUFFER_SIZE)
	for i := 0; i < BUFFER_SIZE; i++ { // inicializa empty com o valor BUFFER\_SIZE
		signal(empty)
	}
	signal(mutex)	// inicializa mutex com o valor 1
	go producer()
	go consumer()
	wait(make(chan int)) // espera indefinidamente
}

