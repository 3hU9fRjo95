/* pc_monitor.go */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
	. "sync"
)

func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

func random_sleep () {
	time.Sleep(random_between(0, MAX_SLEEP_TIME) * 10e7)
}

func signal (channel chan int) {
	if len(channel) < cap(channel) || cap(channel) == 0 {
		channel <- 0
	}
}

func wait (channel chan int) {
	<-channel
}

const (
	MAX_SLEEP_TIME = 5
	BUFFER_SIZE = 5
)

type Monitor struct {
	mutex *Mutex
	empty, full chan int
	buffer []int
	front, rear, count int
}

var monitor *Monitor

func newMonitor () *Monitor {
	return &Monitor {
		new(Mutex),
		make(chan int, BUFFER_SIZE), // semáforo
		make(chan int, BUFFER_SIZE), // semáforo
		make([]int, BUFFER_SIZE),
		0, 0, 0 }
}

func (m *Monitor) produce (i int) {
	m.mutex.Lock()
		for m.count == BUFFER_SIZE { // buffer cheio?
			m.mutex.Unlock()
			wait(m.empty)    // (quase) uma variável de condição
			m.mutex.Lock()
		}
		Println("producing:", i)
		m.buffer[m.rear] = i
		m.rear = (m.rear + 1) % BUFFER_SIZE
		m.count++
		signal(m.full)
	m.mutex.Unlock()
}

func (m *Monitor) consume () {
	m.mutex.Lock()
		for m.count == 0 { // buffer vazio?
			m.mutex.Unlock()
			wait(m.full)		// (quase) uma variável de condição
			m.mutex.Lock()
		}
		v := m.buffer[m.front]
		m.front = (m.front + 1) % BUFFER_SIZE
		m.count--
		Println("\t\t\tconsuming:", v)
		signal(m.empty)
	m.mutex.Unlock()
}

func producer () {
	for i := 0; ; i++ { // laço infinito
		monitor.produce(i)
		random_sleep()
	}
}

func consumer () {
	for { // laço infinito
		monitor.consume()
		random_sleep()
	}
}

func main () {
	monitor = newMonitor()
	go producer()
	go consumer()
	wait(make(chan int)) // espera indefinidamente
}

