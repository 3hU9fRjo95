/* pc_mutex */

package main

import (
	. "fmt"
	"crypto/rand"
	"time"
	. "sync"
)

func random_between (begin, end int64) int64 {
	random_byte := make([]byte, 1)
	rand.Read(random_byte)
	return int64(random_byte[0]) % (end - begin + 1) + begin
}

func random_sleep () {
	time.Sleep(random_between(0, MAX_SLEEP_TIME) * 10e7)
}

func signal (channel chan int) {
	if len(channel) < cap(channel) || cap(channel) == 0 {
		channel <- 0
	}
}

func wait (channel chan int) {
	<-channel
}

const (
	MAX_SLEEP_TIME = 5 // tempo máximo de sleep
	BUFFER_SIZE = 5		 // tamanho do buffer
)

var (
	mutex = new(Mutex)
	buffer []int
	count, front, rear = 0, 0, 0
)

func producer () {
	for i := 0; ; i++ { // laço infinito
		for count == BUFFER_SIZE { // buffer cheio?
			random_sleep() // (almost) busy wait
		}
		mutex.Lock()
			if count < BUFFER_SIZE  {
				Println("producing:", i)
				buffer[rear] = i
				rear = (rear + 1) % BUFFER_SIZE
				count++
			} else {
				i--
			}
		mutex.Unlock()
		random_sleep()
	}
}

func consumer () {
	for { // laço infinito
		for count == 0 { // buffer vazio?
			random_sleep() // (almost) busy wait
		}
		mutex.Lock()
			if count > 0 {
				v := buffer[front]
				front = (front + 1) % BUFFER_SIZE
				count--
				Println("\t\t\tconsuming:", v)
			}
		mutex.Unlock()
		random_sleep()
	}
}

func main () {
	buffer = make([]int, BUFFER_SIZE)
	go producer()
	go consumer()
	wait(make(chan int)) // espera indefinidamente
}

