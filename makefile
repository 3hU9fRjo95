PROG = pc_channel pc_mutex pc_sem pc_monitor pc_monitor_cond pc_channel_multi

all: $(PROG)

%: %.8
	8l -o $@ $<

%.8: %.go
	8g $<

